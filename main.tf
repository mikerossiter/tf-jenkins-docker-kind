# Pulls the image
resource "docker_image" "alpine-test" {
  name = "mikerossiter/jenkins-docker-hub"

  build {
    context = "."
  }
}

# # Create a container
# resource "docker_container" "jenkins-docker" {
#  image = docker_image.alpine-test.image_id
#  name  = "jenkins-docker"
# }
